# Diagrama de Gantt

## Histórico de revisões
|   Data   |  Versão  |        Descrição       |          Autor(es)          |
|:--------:|:--------:|:----------------------:|:---------------------------:|
|04/04/2019|   0.1    | Iniciando o documento       |   Joberth Rogers  |

## Sumário
[1. Introdução](#1-introducao) <br>
[2. Diagrama ](#2-diagrama)<br>
[9. Referências ](#3-referencias)

## 1. Introdução

Esse diagrama é uma ferramenta visual no qual é usada para controlar o conograma e estipular os prazos de entrega validado previamente de um determinado projeto. Seu principal objetivo é verificar interdepêndencias entre atividades, mostrar as tarefas do projeto que precisam ser realizadas de uma forma geral no decorrer do tempo de desenvolvimento, distribuir todas as responsabilidades entre o grupo e definir prazos. Dessa forma é possível visualizar o andamento do projeto de forma geral e manter a equipe focada naquilo que deve ser feito. 

## 2. Diagrama

![Diagrama de Gantt](img/DiagramaGantt.jpg)

## 3. Referências

[1] https://www.projectbuilder.com.br/blog/grafico-de-gantt-como-e-por-que-utiliza-lo-para-gerenciar-projetos/